import React, { Component } from 'react';
import { connect } from 'react-redux';
import Fragment from 'render-fragment';

import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';
import CounterInput from '../../components/CounterInput/CounterInput';
import * as actionTypes from '../../store/action';

class Counter extends Component {
    state = {
        number: ''
    }

    // counterChangedHandler = ( action, value ) => {
    //     switch ( action ) {
    //         case 'inc':
    //             this.setState( ( prevState ) => { return { counter: prevState.counter + 1 } } )
    //             break;
    //         case 'dec':
    //             this.setState( ( prevState ) => { return { counter: prevState.counter - 1 } } )
    //             break;
    //         case 'add':
    //             this.setState( ( prevState ) => { return { counter: prevState.counter + value } } )
    //             break;
    //         case 'sub':
    //             this.setState( ( prevState ) => { return { counter: prevState.counter - value } } )
    //             break;
    //     }
    // }
    handleChange(e){
        this.setState({
            number: e.target.value
        })
        // console.log(this.state.number);
    }

    handleSubmit(e){
        e.preventDefault();
        let input = {
            number: this.state.number
        }
        console.log(input);
    }
    render () {
        return (
            <Fragment>
                <CounterOutput value={this.props.ctr} />
                <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
                <CounterControl label="Decrement" clicked={this.props.onDecrementCounter}  />
                <CounterControl label="Add" clicked={this.props.onAdd}  />
                <CounterControl label="Subtract 7" clicked={this.props.onSubtract}  />
                <CounterInput type='text' value={this.state.number} change={(e) => this.handleChange(e)} />
                <CounterInput type='submit'change={(e) => this.handleSubmit(e)} /> 
            </Fragment>

        );
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.counter
        
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch({type: actionTypes.INCREMENT}),
        onDecrementCounter: () => dispatch({type: actionTypes.DECREMENT}),
        onAdd: () => dispatch ({type: actionTypes.ADD, value: this.state.number}),
        onSubtract: () => dispatch ({type: actionTypes.SUBTRACT, value:7})
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);