import * as actionTypes from './action';


const initialState = {
    counter: 0,
    // number: ''
}

const reducer = (state = initialState, action) => {
    // if (action.type === actionTypes.INPUT){
    //     return{
    //         number:state.number.target.value
    //     }
    // }
    if (action.type === actionTypes.INCREMENT) {
        return {
            counter: state.counter + 1
        }
    }
    if (action.type === actionTypes.DECREMENT){
        return {
            counter: state.counter - 1
        }
    }
    if (action.type === actionTypes.ADD ){
        console.log(action.value)
        return{
            counter: state.counter + action.value
        }
        
    }
    if (action.type === actionTypes.SUBTRACT){
        return {
            counter: state.counter - action.value
        }
    }
    return state;
};

export default reducer;