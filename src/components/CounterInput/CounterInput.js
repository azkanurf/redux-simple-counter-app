import React from 'react';

const counterInput = (props) => (
    <div className="counterInput">
        <input type={props.type} onChange = {props.change} value={props.value} />
    </div>
) 

export default counterInput;